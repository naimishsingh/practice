import React, {Component} from 'react';
import {Button, FormGroup, FormControl, ControlLabel} from 'react-bootstrap';
import './Login.css';

class Login extends Component{
    constructor(){
        super();
        this.state = {
            name : '',
            email : ''
        }
        this.setEmail = this.setEmail.bind(this);
        this.setPassword = this.setPassword.bind(this);
    }

    handleSubmit(e){
        e.preventDefault();
    }

    setEmail(e){
        this.setState({
            email : e.target.value
        })
    }

    setPassword(e){
        this.setState({
            password : e.target.value
        })
    }

    validateForm(){
        return (this.password.length > 0) && (this.email.length > 0);
    }

    render(){
        return(
            <div className='Login'>
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controilId = 'email' bsSize = 'large'>
                        <ControlLabel>Email</ControlLabel>
                        <FormControl 
                            autoFocus
                            type = 'email'
                            value = {this.email}
                            onChange = {this.setEmail}
                        />
                    </FormGroup>
                    <FormGroup controilId='password' bsSize='large'>
                        <ControlLabel>Password</ControlLabel>
                        <FormControl 
                            value={this.password}
                            type='password'
                            onChange={this.setPassword}
                        />
                    </FormGroup>
                    <Button block bsSize='large' disabled={!this.validateForm} type='submit'>
                        Login
                    </Button>
                </form>
            </div>
        )
    }
}

export default Login;