import React, { Component } from "react";
import {
  HelpBlock,
  FormGroup,
  FormControl,
  ControlLabel,
  Button,
} from "react-bootstrap";

class Signup extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      confirmPassword: "",
    };
  }

  handleSubmit() {}

  handleChange() {}

  handleFieldChange() {}

  render() {
    return (
      <div className='Signup'>
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email" bsSize="large">
            <ControlLabel>Email</ControlLabel>
            <FormControl
              autofoucs
              type="email"
              value={this.email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              type="password"
              value={this.password}
              onChange={this.handleFieldChange}
            />
          </FormGroup>
          <FormGroup controlId="confirmPassword" bsSize="large">
            <ControlLabel>Confirm Password</ControlLabel>
            <FormControl
              type="password"
              value={this.confirmPassword}
              onChange={this.handleFieldChange}
            />
          </FormGroup>
          <Button block type="submit" bsSize="large">
            Signup
          </Button>
        </form>
      </div>
    );
  }
}

export default Signup;
